package com.mars.module.admin.service;

import com.mars.module.admin.entity.ApTest1;
import com.mars.common.response.PageInfo;
import com.mars.module.admin.request.ApTest1Request;

import java.util.List;

/**
 * 测试1接口
 *
 * @author mars
 * @date 2024-03-01
 */
public interface IApTest1Service {
    /**
     * 新增
     *
     * @param param param
     * @return ApTest1
     */
    ApTest1 add(ApTest1Request param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(Long id);

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<Long> ids);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(ApTest1Request param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return ApTest1
     */
    ApTest1 getById(Long id);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<ApTest1>
     */
    PageInfo<ApTest1> pageList(ApTest1Request param);


    /**
     * 查询所有数据
     *
     * @return List<ApTest1>
     */
    List<ApTest1> list(ApTest1Request param);
}
