package com.mars.module.admin.entity;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.*;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.mars.module.system.entity.BaseEntity;

/**
 * 请假对象 biz_leave
 *
 * @author mars
 * @date 2024-01-25
 */

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "请假对象")
@Builder
@Accessors(chain = true)
@TableName("biz_leave")
public class BizLeave extends BaseEntity {

    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;
    /**
     * 请假类型
     */
    @Excel(name = "请假类型")
    @ApiModelProperty(value = "请假类型")
    private String type;
    /**
     * 标题
     */
    @Excel(name = "标题")
    @ApiModelProperty(value = "标题")
    private String title;
    /**
     * 原因
     */
    @Excel(name = "原因")
    @ApiModelProperty(value = "原因")
    private String reason;
    /**
     * 开始时间
     */
    @Excel(name = "开始时间")
    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startTime;
    /**
     * 结束时间
     */
    @Excel(name = "结束时间")
    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endTime;
    /**
     * 请假时长
     */
    @Excel(name = "请假时长")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "请假时长")
    private String totalTime;
    /**
     * 申请人
     */
    @Excel(name = "申请人")
    @ApiModelProperty(value = "申请人")
    private String applyUser;
    /**
     * 申请时间
     */
    @Excel(name = "申请时间")
    @ApiModelProperty(value = "申请时间")
    private LocalDateTime applyTime;
    /**
     * 实际开始时间
     */
    @Excel(name = "实际开始时间")
    @ApiModelProperty(value = "实际开始时间")
    private LocalDateTime realityStartTime;
    /**
     * 实际结束时间
     */
    @Excel(name = "实际结束时间")
    @ApiModelProperty(value = "实际结束时间")
    private LocalDateTime realityEndTime;

    /**
     * 流程状态
     */
    @ApiModelProperty(value = "1 未提交 2 流程中 3 已完成")
    private Integer proStatus;


    /**
     * 代办任务名称
     */
    @TableField(exist = false)
    private String taskName;
    /**
     * 流程实例ID
     */
    private String proInstanceId;

    /**
     * 任务ID
     */

    @TableField(exist = false)
    private String taskId;
    /**
     * 业务key
     */
    @TableField(exist = false)
    private String businessKey;
}
