package com.mars.module.admin.service;

import com.mars.module.admin.entity.BizLeave;
import com.mars.common.response.PageInfo;
import com.mars.module.admin.request.BizLeaveRequest;
import com.mars.module.workflow.request.ApproveRequest;
import com.mars.module.workflow.request.StartRequest;

import java.util.List;

/**
 * 请假接口
 *
 * @author mars
 * @date 2024-01-25
 */
public interface IBizLeaveService {
    /**
     * 新增
     *
     * @param param param
     * @return BizLeave
     */
    BizLeave add(BizLeaveRequest param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(Long id);

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<Long> ids);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(BizLeaveRequest param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return BizLeave
     */
    BizLeave getById(Long id);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<BizLeave>
     */
    PageInfo<BizLeave> pageList(BizLeaveRequest param);


    /**
     * 查询所有数据
     *
     * @return List<BizLeave>
     */
    List<BizLeave> list(BizLeaveRequest param);

    /**
     * 请假流程提交
     *
     * @param request request
     */
    void submitProcess(StartRequest request);

    /**
     * 审批完成提交
     *
     * @param request 请求参数
     */
    void submitApprove(ApproveRequest request);

}
