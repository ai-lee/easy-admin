package com.mars.module.workflow.mapper;

import com.mars.module.workflow.response.DeploymentResponse;
import com.mars.module.workflow.response.ProcessResponse;
import com.mars.module.workflow.response.ToDoTaskResponse;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * @Description: 流程引擎相关接口
 * @Author: mars
 * @Date: 2022-12-27
 * @Version: V1.0
 */
public interface WorkFlowMapper {

    /**
     * 查询已经发起的流程
     *
     * @param businessKey businessKey
     * @return List<ProcessVo>
     */
    List<ProcessResponse> selectProcessByBusinessKey(@Param("businessKey") String businessKey);

    /**
     * 通过用户ID查询代办
     *
     * @param assignee 用户ID
     * @return List<ToDoTaskResponse>
     */
    List<ToDoTaskResponse> selectTodoTaskListByAssignee(@Param("assignee") String assignee);

    /**
     * 通过流程实例ID查询代办信息
     *
     * @param proInstanceIds proInstanceIds
     * @return List<ToDoTaskResponse>
     */
    List<ToDoTaskResponse> selectTodoTaskListByInstanceId(@Param("proInstanceIds") List<String> proInstanceIds);

    /**
     * 通过业务ID查询流程key
     *
     * @param businessKey businessKey
     * @return String
     */
    String selectProcessKeyByBusinessKey(@Param("businessKey") String businessKey);

    /**
     * 通过流程实例ID查询流程key
     *
     * @param proInstanceId proInstanceId
     * @return String
     */
    String selectProcessKeyByInstanceId(@Param("proInstanceId") String proInstanceId);

    /**
     * 批量查询部署信息
     *
     * @param ids ids
     * @return List<Deployment>
     */
    List<DeploymentResponse> selectDeploymentByIds(@Param("ids") List<String> ids);

}
