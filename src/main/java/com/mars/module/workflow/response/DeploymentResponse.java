package com.mars.module.workflow.response;

import lombok.Data;
import org.activiti.engine.repository.Deployment;

import java.util.Date;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2024-02-02 18:05:29
 */
@Data
public class DeploymentResponse {

    /**
     * 部署ID
     */
    private String id;

    /**
     * 部署名称
     */
    private String name;

    private Date deploymentTime;

    private String category;

    private String url;

    private String tenantId;

}
