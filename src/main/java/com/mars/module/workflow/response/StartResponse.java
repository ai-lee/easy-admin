package com.mars.module.workflow.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author WANGYUTAO28
 * @create 2021 - 04 - 20 15:26
 */
@Data
@NoArgsConstructor
@ApiModel(description = "流程启动响应")
public class StartResponse implements Serializable {
    private static final long serialVersionUID = -1882016346092020477L;

    @ApiModelProperty(value = "流程实例id")
    private Long procInstId;

    @ApiModelProperty(value = "当前任务id")
    private String taskId;
}
