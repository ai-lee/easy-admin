package com.mars.module.workflow.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.activiti.bpmn.model.FlowNode;
import org.activiti.engine.task.TaskInfo;

import java.util.Date;
import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@TableName("ACT_HI_OPINION")
public class ActHiOpinion {

    @TableId("ID_")
    private String id;

    @TableField("TYPE_")
    private String type;

    @TableField("CURRENT_NODE_")
    private String currentNode;

    @TableField("GLOBAL_NODE_")
    private String globalNode;

    @TableField("CURRENT_NODE_NAME_")
    private String currentNodeName;

    @TableField("GLOBAL_NODE_NAME_")
    private String globalNodeName;

    @TableField("PROC_INST_ID_")
    private String procInstId;

    @TableField("TASK_ID_")
    private String taskId;

    @TableField("EXECUTION_ID_")
    private String executionId;

    @TableField("USER_ID_")
    private String userId;

    @TableField("CREATE_TIME_")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("LAST_UPDATE_TIME_")
    private Date lastUpdateTime;

    @TableField("TENANT_ID_")
    private String tenantId;

    @TableField("OPINION_")
    private String opinion;

    @TableField("ATTACHMENT_NAME_")
    private String attachmentName;

    @TableField("ATTACHMENT_URL_")
    private String attachmentUrl;

    @TableField("REMARK_")
    private String remark;

    /**
     * only for start end event
     */
    public ActHiOpinion(String type, FlowNode flowNode, String procInstId, String userId, String opinion) {
        this.id = UUID.randomUUID().toString();
        this.type = type;
        this.currentNode = flowNode.getId();
        this.currentNodeName = flowNode.getName();
        this.procInstId = procInstId;
        this.userId = userId;
        this.createTime = new Date();
        this.lastUpdateTime = new Date();
        this.opinion = opinion;
    }

    /**
     * 用于结束日志操作
     *
     * @param type
     * @param flowNode
     * @param procInstId
     * @param userId
     * @param opinion
     * @param createTime
     */
    public ActHiOpinion(String type, FlowNode flowNode, String procInstId, String userId, String opinion, Date createTime) {
        this.id = UUID.randomUUID().toString();
        this.type = type;
        this.currentNode = flowNode.getId();
        this.currentNodeName = flowNode.getName();
        this.procInstId = procInstId;
        this.userId = userId;
        this.createTime = createTime;
        this.lastUpdateTime = createTime;
        this.opinion = opinion;
    }

    /**
     * @param task           任务
     * @param type           操作类型
     * @param globalNode     只有在驳回直送的时候才填写该值
     * @param opinion        意见
     * @param attachmentName 附件名称
     * @param attachmentUrl  附件地址
     */
    public ActHiOpinion(TaskInfo task, String type, String globalNode, String opinion, String attachmentName, String attachmentUrl) {
        this.id = UUID.randomUUID().toString();
        this.type = type;
        this.currentNode = task.getTaskDefinitionKey();
        this.currentNodeName = task.getName();
        this.globalNode = globalNode;
        this.procInstId = task.getProcessInstanceId();
        this.taskId = task.getId();
        this.executionId = task.getExecutionId();
        this.userId = task.getAssignee();
        this.createTime = new Date();
        this.lastUpdateTime = new Date();
        this.opinion = opinion;
        this.attachmentName = attachmentName;
        this.attachmentUrl = attachmentUrl;
    }

    /**
     * only for recall
     */
    public ActHiOpinion(TaskInfo task, String type, String userId, String opinion) {
        this.id = UUID.randomUUID().toString();
        this.type = type;
        this.currentNode = task.getTaskDefinitionKey();
        this.currentNodeName = task.getName();
        this.procInstId = task.getProcessInstanceId();
        this.executionId = task.getExecutionId();
        this.taskId = task.getId();
        this.userId = userId;
        this.createTime = new Date();
        this.lastUpdateTime = new Date();
        this.opinion = opinion;
    }
}
