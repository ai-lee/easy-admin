package com.mars.module.workflow.controller;


import com.mars.common.enums.BusinessType;
import com.mars.common.response.PageInfo;
import com.mars.common.result.R;
import com.mars.framework.annotation.Log;
import com.mars.module.workflow.request.ListModelRequest;
import com.mars.module.workflow.request.ModelRequest;
import com.mars.module.workflow.service.IProcessModelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.repository.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Hp
 */
@Slf4j
@RestController
@AllArgsConstructor
@Api(value = "流程引擎模型管理", tags = "流程引擎模型管理")
@RequestMapping("/workflow")
public class ProModelController {

    private final IProcessModelService repositoryService;

    /**
     * 模型列表
     *
     * @param request request
     * @return R<List < Model>>
     */
    @ApiOperation(value = "模型列表")
    @PostMapping("/modeler/list")
    public R<PageInfo<Model>> list(@RequestBody ListModelRequest request) {
        return R.success(repositoryService.getModelList(request));
    }

    /**
     * 创建模型
     */
    @ApiOperation(value = "创建模型")
    @PostMapping(value = "/modeler/create")
    public R<String> create(@RequestBody ModelRequest request) {
        repositoryService.create(request);
        return R.success();
    }

    /**
     * 根据Model部署流程
     */
    @ApiOperation(value = "部署流程")
    @PostMapping(value = "/modeler/deploy")
    public R<Void> deploy(@RequestBody ModelRequest request) throws IOException {
        repositoryService.deploy(request);
        return R.success();
    }

    /**
     * 导出model的xml文件
     */
    @ApiOperation(value = "导出model的xml文件")
    @PostMapping(value = "/modeler/export")
    public void export(@RequestBody ModelRequest request, HttpServletResponse response) throws IOException {
        repositoryService.export(request, response);
    }

    /**
     * 删除流程模型
     *
     * @param request request
     * @return R
     */
    @Log(title = "删除流程模型", businessType = BusinessType.DELETE)
    @PostMapping("/modeler/remove")
    public R<Void> remove(@RequestBody ModelRequest request) {
        repositoryService.remove(request.getIds());
        return R.success();
    }

}
