package com.mars.module.workflow.controller;


import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.mars.common.enums.BusinessType;
import com.mars.common.result.R;
import com.mars.common.util.ExcelUtils;
import io.swagger.annotations.Api;
import com.mars.framework.annotation.RateLimiter;
import com.mars.module.workflow.entity.ProcessNodeAssignee;
import com.mars.framework.annotation.Log;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import com.mars.common.response.PageInfo;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.workflow.service.IProcessNodeAssigneeService;
import com.mars.module.workflow.request.ProcessNodeAssigneeRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * 审批人员控制层
 *
 * @author mars
 * @date 2024-01-25
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "审批人员接口管理",tags = "审批人员接口管理")
@RequestMapping("/admin/processNodeAssignee" )
public class ProcessNodeAssigneeController {

    private final IProcessNodeAssigneeService iProcessNodeAssigneeService;

    /**
     * 分页查询审批人员列表
     */
    @ApiOperation(value = "分页查询审批人员列表")
    @PostMapping("/pageList")
    public R<PageInfo<ProcessNodeAssignee>> pageList(@RequestBody ProcessNodeAssigneeRequest processNodeAssigneeRequest) {
        return R.success(iProcessNodeAssigneeService.pageList(processNodeAssigneeRequest));
    }

    /**
     * 获取审批人员详细信息
     */
    @ApiOperation(value = "获取审批人员详细信息")
    @GetMapping(value = "/query/{id}")
    public R<ProcessNodeAssignee> detail(@PathVariable("id") Long id) {
        return R.success(iProcessNodeAssigneeService.getById(id));
    }

    /**
     * 新增审批人员
     */
    @Log(title = "新增审批人员", businessType = BusinessType.INSERT)
    @RateLimiter
    @ApiOperation(value = "新增审批人员")
    @PostMapping("/add")
    public R<Void> add(@RequestBody ProcessNodeAssigneeRequest processNodeAssigneeRequest) {
        iProcessNodeAssigneeService.add(processNodeAssigneeRequest);
        return R.success();
    }

    /**
     * 修改审批人员
     */
    @Log(title = "修改审批人员", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改审批人员")
    @PostMapping("/update")
    public R<Void> edit(@RequestBody ProcessNodeAssigneeRequest processNodeAssigneeRequest) {
        iProcessNodeAssigneeService.update(processNodeAssigneeRequest);
        return R.success();
    }

    /**
     * 删除审批人员
     */
    @Log(title = "删除审批人员", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除审批人员")
    @PostMapping("/delete/{ids}")
    public R<Void> remove(@PathVariable Long[] ids) {
        iProcessNodeAssigneeService.deleteBatch(Arrays.asList(ids));
        return R.success();
    }

    /**
    * 导出审批人员
    *
    * @param response response
    * @throws IOException IOException
    */
    @PostMapping(value = "/export")
    public void exportExcel(HttpServletResponse response,@RequestBody ProcessNodeAssigneeRequest processNodeAssigneeRequest) throws IOException {
        List<ProcessNodeAssignee> list = iProcessNodeAssigneeService.list(processNodeAssigneeRequest);
        ExcelUtils.exportExcel(list, ProcessNodeAssignee.class, "审批人员信息", response);
    }
}
