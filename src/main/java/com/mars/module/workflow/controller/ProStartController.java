package com.mars.module.workflow.controller;

import com.mars.common.result.R;
import com.mars.module.workflow.request.StartRequest;
import com.mars.module.workflow.response.StartResponse;
import com.mars.module.workflow.service.IProStartApproveService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Hp
 */
@Api(value = "流程发起", tags = {"流程发起"})
@RestController
@RequestMapping("/workflow")
@AllArgsConstructor
public class ProStartController {

    private final IProStartApproveService proStartService;

    /**
     * 流程发起
     *
     * @param request request
     * @return R<StartResponse>
     */
    @ApiOperation(value = "流程发起", notes = "流程发起")
    @PostMapping(value = "/startProcessInstanceByKey")
    public R<StartResponse> startProcessInstanceByKey(@Validated @RequestBody StartRequest request) {
        return R.success(proStartService.startProcess(request));
    }
}
