package com.mars.module.workflow.service;

import com.mars.module.workflow.request.NextStepRequest;
import com.mars.module.workflow.response.FlowElementResponse;
import com.mars.module.workflow.response.NextNodeResponse;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.FlowNode;

import java.util.Collection;
import java.util.List;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2024-02-01 09:20:30
 */
public interface IProcessNodeService {

    /**
     * 查询下一个节点信息
     *
     * @param taskId 任务ID
     * @return List<NextNodeResponse>
     */
    FlowElement queryNextStepInfo(String taskId);

    /**
     * 获取流程节点列表
     *
     * @param deploymentId 部署ID
     * @return Collection<FlowElement>
     */
    List<FlowElementResponse> getNodeList(String deploymentId, String definitionKey);
}
