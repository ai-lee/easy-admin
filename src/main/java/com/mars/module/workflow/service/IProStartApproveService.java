package com.mars.module.workflow.service;

import com.mars.module.workflow.request.ApproveRequest;
import com.mars.module.workflow.request.StartRequest;
import com.mars.module.workflow.response.StartResponse;

/**
 * 流程启动服务
 *
 * @author 程序员Mars
 */
public interface IProStartApproveService {
    /**
     * 启动流程
     *
     * @param request 启动流程请求参数
     * @return Map<String, String>
     */
    StartResponse startProcess(StartRequest request);

    /**
     * 完成任务
     *
     * @param request request
     * @return 是否还有未完成的任务
     */
    boolean submitApprove(ApproveRequest request);

    String getNextNodeId(String proInstanceId);
}
