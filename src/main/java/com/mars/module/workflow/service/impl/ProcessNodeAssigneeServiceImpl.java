package com.mars.module.workflow.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.common.response.PageInfo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.workflow.request.ProcessNodeAssigneeRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.mars.module.workflow.mapper.ProcessNodeAssigneeMapper;
import org.springframework.beans.BeanUtils;
import com.mars.module.workflow.entity.ProcessNodeAssignee;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.module.workflow.service.IProcessNodeAssigneeService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 审批人员业务层处理
 *
 * @author mars
 * @date 2024-01-25
 */
@Slf4j
@Service
@AllArgsConstructor
public class ProcessNodeAssigneeServiceImpl implements IProcessNodeAssigneeService {

    private final ProcessNodeAssigneeMapper baseMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ProcessNodeAssignee add(ProcessNodeAssigneeRequest request) {
        ProcessNodeAssignee entity = ProcessNodeAssignee.builder().build();
        BeanUtils.copyProperties(request, entity);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(Long id) {
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBatch(List<Long> ids) {
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean update(ProcessNodeAssigneeRequest request) {
        ProcessNodeAssignee entity = ProcessNodeAssignee.builder().build();
        BeanUtils.copyProperties(request, entity);
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public ProcessNodeAssignee getById(Long id) {
        return baseMapper.selectById(id);
    }

    @Override
    public PageInfo<ProcessNodeAssignee> pageList(ProcessNodeAssigneeRequest request) {
        Page<ProcessNodeAssignee> page = new Page<>(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<ProcessNodeAssignee> query = this.buildWrapper(request);
        IPage<ProcessNodeAssignee> pageRecord = baseMapper.selectPage(page, query);
        return PageInfo.build(pageRecord);
    }


    @Override
    public List<ProcessNodeAssignee> list(ProcessNodeAssigneeRequest request) {
        LambdaQueryWrapper<ProcessNodeAssignee> query = this.buildWrapper(request);
        return baseMapper.selectList(query);
    }

    @Override
    public List<ProcessNodeAssignee> getNodeAssigneeList(String flowKey) {
        return baseMapper.selectList(Wrappers.lambdaQuery(ProcessNodeAssignee.class).eq(ProcessNodeAssignee::getFlowKey, flowKey));
    }

    private LambdaQueryWrapper<ProcessNodeAssignee> buildWrapper(ProcessNodeAssigneeRequest param) {
        LambdaQueryWrapper<ProcessNodeAssignee> query = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(param.getFlowKey())) {
            query.like(ProcessNodeAssignee::getFlowKey, param.getFlowKey());
        }
        if (StringUtils.isNotBlank(param.getNodeId())) {
            query.like(ProcessNodeAssignee::getNodeId, param.getNodeId());
        }
        if (StringUtils.isNotBlank(param.getAssignee())) {
            query.like(ProcessNodeAssignee::getAssignee, param.getAssignee());
        }
        if (param.getAssigneeType() != null) {
            query.eq(ProcessNodeAssignee::getAssigneeType, param.getAssigneeType());
        }
        return query;
    }

}
