package com.mars.module.workflow.config;

import org.activiti.bpmn.model.Process;
import org.activiti.bpmn.model.*;
import org.activiti.image.impl.DefaultProcessDiagramCanvas;
import org.activiti.image.impl.DefaultProcessDiagramGenerator;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;

/**
 * @author 自定义流程图管理器
 */
public class MyDefaultProcessDiagramGenerator extends
        DefaultProcessDiagramGenerator {

    public InputStream generateDiagram(BpmnModel bpmnModel, String imageType,
                                       List highLightedActivities, List highLightedFlows,
                                       String activityFontName, String labelFontName,
                                       String annotationFontName, ClassLoader customClassLoader,
                                       double scaleFactor) {

        return generateProcessDiagram(bpmnModel, imageType,
                highLightedActivities, highLightedFlows, activityFontName,
                labelFontName, annotationFontName, customClassLoader,
                scaleFactor).generateImage(imageType);
    }

    public InputStream generateDiagram(BpmnModel bpmnModel, String imageType,
                                       List highLightedActivities) {
        return generateDiagram(bpmnModel, imageType, highLightedActivities,
                Collections.emptyList());
    }

    public InputStream generateDiagram(BpmnModel bpmnModel, String imageType,
                                       List highLightedActivities, List highLightedFlows) {
        return generateDiagram(bpmnModel, imageType, highLightedActivities,
                highLightedFlows, null, null, null, null, 1.0);
    }

    protected DefaultProcessDiagramCanvas generateProcessDiagram(
            BpmnModel bpmnModel, String imageType,
            List highLightedActivities, List highLightedFlows,
            String activityFontName, String labelFontName,
            String annotationFontName, ClassLoader customClassLoader,
            double scaleFactor) {

        prepareBpmnModel(bpmnModel);

        DefaultProcessDiagramCanvas processDiagramCanvas = initProcessDiagramCanvas(
                bpmnModel, imageType, activityFontName, labelFontName,
                annotationFontName, customClassLoader);

        // Draw pool shape, if process is participant in collaboration
        for (Pool pool : bpmnModel.getPools()) {
            GraphicInfo graphicInfo = bpmnModel.getGraphicInfo(pool.getId());
            processDiagramCanvas.drawPoolOrLane(pool.getName(), graphicInfo);
        }

        // Draw lanes
        for (Process process : bpmnModel.getProcesses()) {
            for (Lane lane : process.getLanes()) {
                GraphicInfo graphicInfo = bpmnModel
                        .getGraphicInfo(lane.getId());
                processDiagramCanvas
                        .drawPoolOrLane(lane.getName(), graphicInfo);
            }
        }

        // Draw activities and their sequence-flows
        for (Process process : bpmnModel.getProcesses()) {
            for (FlowNode flowNode : process
                    .findFlowElementsOfType(FlowNode.class)) {
                drawActivity(processDiagramCanvas, bpmnModel, flowNode,
                        highLightedActivities, highLightedFlows, scaleFactor);
            }
        }

        // Draw artifacts
//		for (Process process : bpmnModel.getProcesses()) {
//
//			for (Artifact artifact : process.getArtifacts()) {
//				drawArtifact(processDiagramCanvas, bpmnModel, artifact);
//			}
//
//			List subProcesses = process.findFlowElementsOfType(
//					SubProcess.class);
//			if (subProcesses != null) {
//				for (SubProcess subProcess : subProcesses) {
//					for (Artifact subProcessArtifact : subProcess
//							.getArtifacts()) {
//						drawArtifact(processDiagramCanvas, bpmnModel,
//								subProcessArtifact);
//					}
//				}
//			}
//		}
        return processDiagramCanvas;
    }
}
