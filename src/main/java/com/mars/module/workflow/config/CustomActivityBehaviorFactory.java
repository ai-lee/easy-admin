package com.mars.module.workflow.config;

import org.activiti.bpmn.model.ExclusiveGateway;
import org.activiti.engine.impl.bpmn.behavior.ExclusiveGatewayActivityBehavior;
import org.activiti.engine.impl.bpmn.parser.factory.DefaultActivityBehaviorFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author WANGYUTAO28
 * @crete 2021 - 08 - 04 14:53
 */
@Configuration
public class CustomActivityBehaviorFactory extends DefaultActivityBehaviorFactory {

    private CustomExclusiveGatewayActivityBehavior customExclusiveGatewayActivityBehavior;


    /**
     * 通过Spring容器注入新的分支条件行为执行类
     */
    @Bean
    public CustomExclusiveGatewayActivityBehavior customExclusiveGatewayActivityBehavior() {
        return new CustomExclusiveGatewayActivityBehavior();
    }
//    public void setExclusiveGatewayActivityBehaviorExt(CustomExclusiveGatewayActivityBehavior customExclusiveGatewayActivityBehavior) {
//        this.customExclusiveGatewayActivityBehavior = customExclusiveGatewayActivityBehavior;
//    }

    //重写父类中的分支条件行为执行类
    @Override
    public ExclusiveGatewayActivityBehavior createExclusiveGatewayActivityBehavior(ExclusiveGateway exclusiveGateway) {
        return customExclusiveGatewayActivityBehavior;
    }
}
