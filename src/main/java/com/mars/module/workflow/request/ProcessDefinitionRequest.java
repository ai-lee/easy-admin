package com.mars.module.workflow.request;

import com.mars.common.request.PageRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2024-01-23 18:12:10
 */
@Data
@ApiModel(value = "流程定义")
public class ProcessDefinitionRequest extends PageRequest {

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "key")
    private String key;

    @ApiModelProperty(value = "分类")
    private String category;

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "状态")
    private String suspendState;

    @ApiModelProperty(value = "流程部署ID")
    private String deploymentId;


}
