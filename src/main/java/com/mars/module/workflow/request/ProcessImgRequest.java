package com.mars.module.workflow.request;

import com.mars.common.request.PageRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2024-01-23 18:12:10
 */
@Data
@ApiModel(value = "流程定义")
public class ProcessImgRequest {

    @ApiModelProperty(value = "流程实例ID")
    private String proInstanceId;


}
