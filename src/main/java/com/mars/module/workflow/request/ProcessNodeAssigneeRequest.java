package com.mars.module.workflow.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.mars.common.request.PageRequest;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 审批人员请求对象 process_node_assignee
 *
 * @author mars
 * @date 2024-01-25
 */
@Data
@ApiModel(value = "审批人员对象")
@EqualsAndHashCode(callSuper = true)
public class ProcessNodeAssigneeRequest extends PageRequest{



    /**
     * 主键
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 流程key
     */
    @ApiModelProperty(value = "流程key")
    private String flowKey;

    /**
     * 审批人变量
     */
    @ApiModelProperty(value = "审批人变量")
    private String assigneeVariable;

    /**
     * 流程节点ID
     */
    @ApiModelProperty(value = "流程节点ID")
    private String nodeId;

    /**
     * 关联ID
     */
    @ApiModelProperty(value = "关联ID")
    private String assignee;

    /**
     * 类别
     */
    @ApiModelProperty(value = "类别")
    private Integer assigneeType;

    /**
     * 创建时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    /**
     * 创建人名称
     */
    @ApiModelProperty(value = "创建人名称")
    private String createByName;

    /**
     * 修改时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    /**
     * 更新人名称
     */
    @ApiModelProperty(value = "更新人名称")
    private String updateByName;
}
